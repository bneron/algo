from graph.graph_3 import UnDirectedGraph, DirectedGraph, Node, Edge
from graph.traversing import BFS, DFS


print("=============== UnDirected weight Graph =============")
# We want to create a toy graph (not directed) to check our algos
graph_schema = """
#              no
#            1/  \\3
#            n1  n3
#           2| 15/|4
#            n2  n4
#            5\  /6
#              n5
"""

print(graph_schema)

nodes = [Node() for _ in range(6)]
g = UnDirectedGraph()

g.add_edge(nodes[0], nodes[1], weight=1)
g.add_edge(nodes[0], nodes[3], weight=3)
g.add_edge(nodes[1], nodes[2], weight=2)
g.add_edge(nodes[2], nodes[5], weight=5)
g.add_edge(nodes[3], nodes[4], weight=4)
g.add_edge(nodes[4], nodes[5], weight=6)
g.add_edge(nodes[3], nodes[2], weight=15)
print("BFS")
for n, p in BFS(g, nodes[0]):
    print(n, [str(e) for e in p])
print(f"distance = {sum([e.weight for e in p])}")

print("=" * 20, "UnDirected weight Graph 2", "=" * 20)
graph_schema = """
#              no
#            1/  \\3
#            n1  n3
#           2| X  |4  (3-2 15) (1-4 20)
#            n2  n4
#            5\  /6
#              n5
"""

print(graph_schema)

g.add_edge(nodes[1], nodes[4], weight=20)

print("Order", g.order())
print("Size", g.size())

print("BFS")
for n, p in BFS(g, nodes[0]):
    print(n, [str(e) for e in p])
print(f"distance = {sum([e.weight for e in p])}")

print("DFS")
for n, p in DFS(g, nodes[0]):
    print(n, [str(e) for e in p])
print(f"distance = {sum([e.weight for e in p])}")

print("=============== Directed weighted Graph =============")
graph_schema = """
#              n6
#            1/  \\6
#            v    V
#            n7  n9
#           2|^  ^|7
#            | X  |  (10 -> 7 10) (8 -> 9 8)
#            v   vv
#            n8  n10
#            5\  ^
#              v/4
#              n11
"""

print(graph_schema)

nodes += [Node() for _ in range(6)]
g = DirectedGraph()

g.add_edge(nodes[6], nodes[7], weight=1)
g.add_edge(nodes[7], nodes[8], weight=2)
g.add_edge(nodes[8], nodes[11], weight=5)
g.add_edge(nodes[11], nodes[10], weight=4)
g.add_edge(nodes[6], nodes[9], weight=6)
g.add_edge(nodes[9], nodes[10], weight=7)
g.add_edge(nodes[8], nodes[9], weight=8)
g.add_edge(nodes[7], nodes[10], weight=9)
g.add_edge(nodes[10], nodes[7], weight=10)


print("Order", g.order())
print("Size", g.size())

print("BFS")
for n, p in BFS(g, nodes[6]):
    print(n, [str(e) for e in p])
print(f"distance = {sum([e.weight for e in p])}")

print("DFS")
for n, p in DFS(g, nodes[6]):
    print(n, [str(e) for e in p])
print(f"distance = {sum([e.weight for e in p])}")
