from graph.graph_1 import NDGraph, Node, BFS, DFS

# We want to create a toy graph (not directed) to check our algos
#              no
#             /  \
#            n1  n3
#            |    |
#            n2  n4
#             \  /
#              n5

g = NDGraph()

n0 = Node()
n1 = Node()
n2 = Node()
n3 = Node()
n4 = Node()
n5 = Node()

n1.add_neighbor(n2)
n0.add_neighbor(n1)
n3.add_neighbor(n4)
n0.add_neighbor(n3)
n2.add_neighbor(n5)
n4.add_neighbor(n5)

g.add_node(n0)


# The graph is created we will test

# a Depth First Search
# starting from n0
# the possible solutions are
# n0, n1,n2,n5,n4,n3
# n0, n3, n4, n5, n2, n1

print("DFS")
for n in DFS(g, n0):
    print(n.id)

# a Breadth First Search
# starting n0
# the possible solutions are
# n0, n1, n3, n2, n4, n5
# n0, n3, n1, n2, n4, n5
# n0, n1, n3, n4, n2, n5
# ....

print("BFS")
for n in BFS(g, n0):
    print(n.id)
