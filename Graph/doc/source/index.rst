.. MyGraph documentation master file, created by
   sphinx-quickstart on Fri Sep  6 16:18:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyGraph's documentation!
===================================

Graph API reference
===================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   graph
   algo
   helpers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
