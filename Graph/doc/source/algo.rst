.. MyGraph documentation master file, created by
   sphinx-quickstart on Fri Sep  6 16:18:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _algo:

*******************
Algorithms on graph
*******************

   .. automodule:: graph.traversing
      :members: