We propose to modelize a Non Directed Graph

we must be able to:
 * create a graph
 * add nodes to this graph
 * connect nodes with edges

once this step is done we will implement to graph search algorithms:
 * Depth First Search
 * Breadth First Search

I propose two graph implementation

## The first implementation graph_1.py and main_1.py

* *graph* store *nodes*
* each *node* have an unique id and knows its neighbors
* the BFS and DFS have been implemented with FIFO and LIFO

The interesting thing to see is that **BFS** and **DFS** differ only by using respectively
a **LIFO** or **FIFO**

## The second implementation graph_2.py and main_2.py

* *graph* store nodes, and edges between nodes
* *nodes* knows only its properties here, an unique id

in this secon implementation we recode LIFO and FIFO using a list and benefit that list are iterable
so they can be convert in set easily.

we see that the 2 search path algorithms *BFS* and *DFS* do not change whatever the graph implementation.