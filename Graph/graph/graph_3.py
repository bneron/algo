from typing import Iterator, Dict
import itertools
from abc import ABCMeta, abstractmethod


class Node:
    """
    Graph node representation
    """

    _id = itertools.count(0)

    def __init__(self, **kwargs) -> None:
        self.id: int = next(self._id)
        for k, v in kwargs:
            setattr(self, k, v)


    def __hash__(self) -> int:
        # to be usable in set an object must be hashable
        # so we need to implement __hash__
        # which must return an int unique per object
        # here we have the Node identifier
        return self.id

    def __str__(self) -> str:
        return f"node_{self.id}"

    def __repr__(self) -> str:
        return str(self)


class Edge:
    """
    modelize edge between two nodes, each edge can support properties
    """

    def __init__(self, src: Node, target: Node, directed=True, **kwargs) -> None:
        self.src = src
        self.target = target
        self.directed = directed
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __str__(self):
        """string representation of this edge"""
        return f"{self.src} -{'>' if self.directed else ''} {self.target}"


    def properties(self) -> Dict:
        """the edge properties"""
        return {k: v for k, v in self.__dict__.items()}


class Graph(metaclass=ABCMeta):
    """
    Base class for graphs.
    A graph is a collection of Nodes linked by edges
    """

    def __init__(self) -> None:
        self._nodes: {}

    def add_node(self, node) -> None:
        """
        Add a node to the graph

        :param node: the node to add
        :type node: :class:`Node`
        :return: None
        """
        if node not in self._nodes:
            self._nodes[node] = {}


    def del_node(self, node: Node) -> None:
        """
        Remove Node from Graph

        :param node: the node to add
        :type node: :class:`Node`
        :return: None
        """
        for nbh in self.neighbors(node):
            del self._nodes[nbh][node]
        del self._nodes[node]


    def neighbors(self, node: Node) -> Iterator[Node]:
        """
        return the nodes connected to node

        :param node: the reference node
        :type node: :class:`Node`
        :return: a set of :class:`Node`
        """
        for n in self._nodes[node]:
            yield n


    def nodes(self) -> Iterator[Node]:
        """
        Iterates over all nodes belonging to the graph

        :return: iterator
        """
        for n in self._nodes:
            yield n

    def edges(self, node: Node) -> Iterator[Edge]:
        """
        Iterates over edge connecting to/from this node
        :param node:
        :return:
        """
        for e in self._nodes[node].values():
            yield e

    def has_edge(self, edge) -> bool:
        """
        :param edge:
        :return: True if it exists an edge connecting edge nodes src and target
        """
        return edge.src in self._nodes and edge.target in self._nodes[edge.src]

    @abstractmethod
    def add_edge(self, src: Node, target: Node, **edge_prop) -> None:
        """

        :param edge:
        :return:
        """
        pass

    @abstractmethod
    def del_edge(self, src: Node, target: Node) -> None:
        """
        remove the edge between nodes src and target
        :param src:
        :param target:
        :return:
        """
        pass


    @staticmethod
    @abstractmethod
    def is_directed() -> bool:
        pass


    def order(self):
        """
        The Order of a graph is the number of Nodes in the graph
        :return: 
        """
        return len(self._nodes)

    @abstractmethod
    def size(self):
        """
        The size of a graph is the number of edges in the graph.
        :return:
        """
        size = 0
        for n in self.nodes():
            size += len(self._nodes[n])
        return size


class UnDirectedGraph(Graph):
    """
    To handle UnDirected Graph
    A graph is a collection of Nodes linked by edges
    """

    def __init__(self) -> None:
        self._nodes = {}

    @staticmethod
    def is_directed() -> bool:
        return False

    def size(self) -> int:
        """
        :return: The number of edges of this graph
        """
        return super().size() // 2


    def add_edge(self, node_1, node_2, **prop):
        """
        Add vertex between node1 and all nodes in nodes

        :return: Node
        """
        self.add_node(node_1)
        self.add_node(node_2)
        self._nodes[node_1][node_2] = Edge(node_1, node_2, directed=False, **prop)
        self._nodes[node_2][node_1] = Edge(node_2, node_1, directed=False, **prop)


    def del_edge(self, src: Node, target: Node) -> None:
        """
        remove edge between nodes src and target
        :param src:
        :param target:
        :return:
        """
        del self._nodes[src][target]
        del self._nodes[target][src]


class DirectedGraph(Graph):
    """
    To handle Directed Graph
    A graph is a collection of Nodes linked by edges
    """

    def __init__(self) -> None:
        self._nodes = {}


    @staticmethod
    def is_directed() -> bool:
        return True


    def size(self):
        """
        :return: The number of edges of this graph
        """
        return super().size()


    def add_edge(self, src: Node, target: Node, **prop) -> None:
        """
        Add edge from node *src* to node *target*

        :param src: the src node
        :param target: the target node
        :param prop: the edge properties
        :return: None
        """
        self.add_node(src)
        self.add_node(target)
        self._nodes[src][target] = Edge(src, target, directed=True, **prop)


    def del_edge(self, src: Node, target: Node) -> Node:
        """
        Remove edge between nodes src and target
        :param src:
        :param target:
        :return:
        """
        del self._nodes[src][target]
