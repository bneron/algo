from __future__ import annotations
import itertools
from typing import Iterator, Set, Optional

from .helpers import FIFO, LIFO


class NDGraph:
    """
    To handle Non Directed Graph
    A graph is a collection of Nodes linked by edges
    there are basically to way to implement a graph
     - The graph handles Nodes, each nodes know it's neighbors
     - The graph handles nodes and the edges between nodes
     below I implemented the first version.
    """

    def __init__(self):
        self.nodes: Set[Node] = set()

    def add_node(self, node: Node) -> None:
        self.nodes.add(node)

    def del_node(self, node: Node) -> None:
        self.nodes.remove(node)


class Node:

    _id = itertools.count(0)

    def __init__(self) -> None:
        self.id: int = next(self._id)
        self.neighbors: Set['Node'] = set()

    def __hash__(self) -> int:
        # to be usable in set an object must be hashable
        # so we need to implement __hash__
        # which must returm an int uniq per object
        # here we ad the identifier of the Node
        return self.id

    def add_neighbor(self, node: 'Node') -> None:
        """
        Add one neighbor to this node
        :param node: the node to add
        :return: None
        """
        # the graph is not directed
        # So don't forget to add the reciprocal relation between the 2 nodes
        # if node is a neighbor of self => self is a neighbor of node
        self.neighbors.add(node)
        node.neighbors.add(self)

    def del_neighbor(self, node: 'Node') -> None:
        """
        remove one neighbor to this node
        :param node: the node to add
        :return: None
        """
        # the graph is not directed
        # So don't forget to remove the reciprocal relation between the 2 nodes
        # if node is a neighbor of self => self is a neighbor of node
        self.neighbors.remove(node)
        node.neigbors.remove(self)

    def get_neighbor(self, node_id: int) -> Optional['Node']:
        """

        :param int node_id: the node id
        :return: the node corresponding to id or None if no node corresponding is found
        """
        for n in self.neighbors:
            if n.id == node_id:
                return n


def DFS(graph: NDGraph, node: Node) -> Iterator[Node]:
    """
    **D**epth **F**irst **S**earch.
    We start the path from the node given as argument,
    This node is labeled as 'visited'
    The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit'
    We pick the last element to visit and visit it
    (The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit').
    on so on until there no nodes to visit anymore.

    :param graph:
    :param node:
    :return:
    """
    to_visit = FIFO()
    to_visit.add(node)
    visited = set()
    while to_visit:
        n = to_visit.pop()
        visited.add(n)
        for neighbor in n.neighbors:
            if neighbor not in visited and neighbor not in to_visit:
                to_visit.add(neighbor)
        yield n


def BFS(graph: NDGraph, node: Node) -> Iterator[Node]:
    """
    **B**readth **F**irst **s**earch
    We start the path from the node given as argument,
    This node is labeled as 'visited'
    The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit'
    we pick the **first** element of the nodes to visit and visit it.
    (The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit')
    on so on until there no nodes to visit anymore.

    :param graph:
    :param node:
    :return:
    """
    to_visit = LIFO()
    to_visit.add(node)
    visited = set()
    while to_visit:
        n = to_visit.pop()
        visited.add(n)
        for neighbor in n.neighbors:
            if neighbor not in visited and neighbor not in to_visit:
                to_visit.add(neighbor)
        yield n
