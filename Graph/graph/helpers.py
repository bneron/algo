from abc import ABCMeta, abstractmethod
from typing import Any
from collections import deque


class AbstractQueue(metaclass=ABCMeta):

    def __init__(self) -> None:
        self._queue = deque()

    def __bool__(self) -> bool:
        """
        :return: True if there is any item in the queue, False otherwise.
        """
        return bool(self._queue)

    def __len__(self) -> int:
        """
        :return: The length of the queue
        """
        return len(self._queue)

    def __contains__(self, item: Any) -> bool:
        return item in self._queue


    def __str__(self):
        return f"{self.__class__.__name__}({str(self._queue)[6:-1]})"


    @abstractmethod
    def add(self, item: Any) -> None:
        return None

    @abstractmethod
    def pop(self) -> Any:
        return None


class FIFO(AbstractQueue):
    """
    This structure allow to store any object,
    The First element add to the structure will be the First element removed from it.
    **F**\ irst **I**\ n **F**\ irst **O**\ ut
    """

    def add(self, item: Any) -> Any:
        """
        Add item on the right side

        :param item: The item to add
        """
        self._queue.append(item)


    def pop(self) -> Any:
        """
        Remove and return an element from the right side.
        If no elements are present, raises an IndexError.

        :param item:
        :return: the first element added in the FIFO
        :rtype: Any
        :raise: IndexError if there is no element in the FiFO
        """
        return self._queue.pop()


class LIFO(AbstractQueue):
    """
    This structure allow to store any object,
    The Last element add to the structure will be the First element removed from it.
    **L**\ ast **I**\ n **F**\ irst **O**\ ut
    """

    def add(self, item: Any) -> None:
        self._queue.append(item)


    def pop(self) -> Any:
        """
        Remove and return an element from the right side.
        If no elements are present, raises an IndexError.

        :param item:
        :return: the first element added in the FIFO
        :rtype: Any
        :raise: IndexError if there is no element in the FiFO
        """
        return self._queue.popleft()


