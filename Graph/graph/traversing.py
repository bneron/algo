from typing import Iterator, List, Union, Tuple

from .helpers import LIFO, FIFO
from .graph_3 import Node, Edge, Graph


def _traversing(to_visit: Union[FIFO, LIFO], graph: Graph, node: Node) -> Iterator[Tuple[Node, List[Edge]]]:
    """
    function that traverse the graph starting from node

    :param to_visit:
    :param graph:
    :param node:
    :return: an iterator at each step return the visiting node and the path
    """
    to_visit.add(node)
    visited = set()
    parent = {}  # to store for each node from which node it has been discovered
    path = []
    while to_visit:
        node = to_visit.pop()
        # it is important to add node in visited right now
        # and not a the end of the block
        # otherwise node is not anymore in to_visit and not yet in visited
        # so when we explore neighbors we add it again in to_visit
        visited.add(node)
        for edge in graph.edges(node):
            if edge.target not in visited and edge.target not in to_visit:
                parent[edge.target] = edge
                to_visit.add(edge.target)
        if node in parent:
            path.append(parent[node])  # the starting node has no parent
        yield node, path


def DFS(graph: Graph, node: Node) -> Iterator[Tuple[Node, List[Edge]]]:
    """
    **D**\ epth **F**\ irst **S**\ earch.
    We start the path from the node given as argument,
    This node is labeled as 'visited'
    The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit'
    We pick the last element to visit and visit it
    (The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit').
    on so on until there no nodes to visit anymore.

    :param graph: The graph to traverse
    :param node: The starting node
    :return: iterator on nodes
    """
    return _traversing(FIFO(), graph, node)


def BFS(graph: Graph, node: Node) -> Iterator[Tuple[Node, List[Edge]]]:
    """
    **B**\ readth **F**\ irst **s**\ earch
    We start the path from the node given as argument,
    This node is labeled as 'visited'
    The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit'
    we pick the **first** element of the nodes to visit and visit it.
    (The neighbors of this node which have not been already 'visited' nor 'to visit' are labelled as 'to visit')
    on so on until there no nodes to visit anymore.

    :param graph:
    :param node:
    :return:
    """
    return _traversing(LIFO(), graph, node)
